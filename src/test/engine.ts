import "mocha";
import * as assert from "assert";

import {
    Vector, Matrix,
    vectorVectorMul,
    vectorMatrixMul,
    matrixMatrixMul,
} from '../engine';

const v1: Vector = { x: 1, y: 2, z: 3, t: 4};
const v2: Vector = { x: 5, y: 6, z: 7, t: 8};

const m1: Matrix = {
    x: {x: 1, y: 4, z: 7, t: 1},
    y: {x: 2, y: 2, z: 0, t: -1},
    z: {x: -1, y: 3, z: 2, t: 5},
    t: {x: 0, y: 4, z: 6, t: 2},
}

const v1xm1: Vector = {
    x: 1*1 + 2*4 + 7*3 + 4*1,
    y: 1*2 + 2*2 + 0*3 + 4*(-1),
    z: 1*(-1) + 2*3 + 3*2 + 4*5,
    t: 1*0 + 2*4 + 3*6 + 4*2
};

const m2: Matrix = {
    x: {x: 3, y: 2, z: 0, t: 1},
    y: {x: 4, y: 0, z: 7, t: 3},
    z: {x: 8, y: 2, z: 1, t: 5},
    t: {x: 6, y: 3, z: 3, t: 8},
}
const m1xm2: Matrix = {
    x: {x: 7, y: 20, z: 27, t: 3},
    y: {x: -3, y: 49, z: 60, t: 45},
    z: {x: 11, y: 59, z: 88, t: 21},
    t: {x: 9, y: 71, z: 96, t: 34},    
}

describe('Matrix operations', function () {
    it(`Multiply vector to vector`, () => {
        assert.deepStrictEqual(vectorVectorMul(v1, v2), 1*5+2*6+3*7+4*8);
        assert.deepStrictEqual(vectorVectorMul(v1, v2), vectorVectorMul(v2, v1));
    });
    it(`Multiply vector and matrix`, () => {
        assert.deepStrictEqual(vectorMatrixMul(v1, m1), v1xm1)
    });
    it(`Multiply matrix and matrix`, () => {
        assert.deepStrictEqual(matrixMatrixMul(m1, m2), m1xm2)
    })
})

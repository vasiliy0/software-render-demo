export interface Point2d {
    x: number;
    y: number;
}
export interface Dot {
    x: number;
    y: number;
    z: number;
    t: number;
}
export type Vector = Dot;
export interface Matrix {
    x: Dot;
    y: Dot;
    z: Dot;
    t: Dot;
}
export interface PolyLine {
    color: string;
    dots: Dot[];
}

export function parseRawDataIntoLines(rawData: string) {
    try {
        const lines: PolyLine[] = rawData
            .split("\n")
            .filter(x => x)
            .map(line => {
                const [color, ...dotsText] = line.split(" ");
                if (!color) {
                    throw new Error("Wrong data: no color");
                }
                if (dotsText.length === 0) {
                    throw new Error("No dots!");
                }
                if (dotsText.length === 1) {
                    throw new Error("Only one dot in line!");
                }
                return {
                    color,
                    dots: dotsText.map(dot => {
                        const [xS, yS, zS] = dot.split(",");
                        const x = parseFloat(xS);
                        const y = parseFloat(yS);
                        const z = parseFloat(zS);
                        if (isNaN(x) || isNaN(y) || isNaN(z)) {
                            throw new Error(`Wrong data in points: ${dot}`);
                        }
                        return {
                            x,
                            y,
                            z,
                            t: 1
                        };
                    })
                };
            });
        return lines;
    } catch (e) {
        console.info(e);
        return undefined;
    }
}

export function vectorVectorMul(v1: Vector, v2: Vector): number {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z + v1.t * v2.t;
}

export function vectorMatrixMul(v: Vector, m: Matrix): Vector {
    return {
        x: vectorVectorMul(v, m.x),
        y: vectorVectorMul(v, m.y),
        z: vectorVectorMul(v, m.z),
        t: vectorVectorMul(v, m.t)
    };
}
export function transposeMatrix(m: Matrix): Matrix {
    return {
        x: { x: m.x.x, y: m.y.x, z: m.z.x, t: m.t.x },
        y: { x: m.x.y, y: m.y.y, z: m.z.y, t: m.t.y },
        z: { x: m.x.z, y: m.y.z, z: m.z.z, t: m.t.z },
        t: { x: m.x.t, y: m.y.t, z: m.z.t, t: m.t.t }
    };
}

function matrixMatrixMulBinary(m1: Matrix, m2: Matrix): Matrix {
    const m1t = transposeMatrix(m1);
    return transposeMatrix({
        x: vectorMatrixMul(m1t.x, m2),
        y: vectorMatrixMul(m1t.y, m2),
        z: vectorMatrixMul(m1t.z, m2),
        t: vectorMatrixMul(m1t.t, m2)
    });
}

export function matrixMatrixMul(...matrixes: Matrix[]): Matrix {
    const matrixesToMultiple = matrixes.slice();
    if (matrixesToMultiple.length === 0) {
        throw new Error("No matrixes");
    } else if (matrixesToMultiple.length === 1) {
        return matrixesToMultiple[0];
    } else {
        const [firstMatrix, ...otherMatrixes] = matrixesToMultiple;
        return matrixMatrixMulBinary(
            firstMatrix,
            matrixMatrixMul(...otherMatrixes)
        );
    }
}

export const IDENTITY_MATRIX: Matrix = {
    x: { x: 1, y: 0, z: 0, t: 0 },
    y: { x: 0, y: 1, z: 0, t: 0 },
    z: { x: 0, y: 0, z: 1, t: 0 },
    t: { x: 0, y: 0, z: 0, t: 1 }
};

export function getRotationMatrix(x: number, y: number, z: number): Matrix {
    const xR: Matrix = x
        ? {
              x: { x: 1, y: 0, z: 0, t: 0 },
              y: { x: 0, y: Math.cos(x), z: -Math.sin(x), t: 0 },
              z: { x: 0, y: Math.sin(x), z: Math.cos(x), t: 0 },
              t: { x: 0, y: 0, z: 0, t: 1 }
          }
        : IDENTITY_MATRIX;

    const yR: Matrix = y
        ? {
              x: { x: Math.cos(y), y: 0, z: Math.sin(y), t: 0 },
              y: { x: 0, y: 1, z: 0, t: 0 },
              z: { x: -Math.sin(y), y: 0, z: Math.cos(y), t: 0 },
              t: { x: 0, y: 0, z: 0, t: 1 }
          }
        : IDENTITY_MATRIX;

    const zR: Matrix = z
        ? {
              x: { x: Math.cos(z), y: -Math.sin(z), z: 0, t: 0 },
              y: { x: Math.sin(z), y: Math.cos(z), z: 0, t: 0 },
              z: { x: 0, y: 0, z: 1, t: 0 },
              t: { x: 0, y: 0, z: 0, t: 1 }
          }
        : IDENTITY_MATRIX;

    return matrixMatrixMul(xR, yR, zR);
}

export function getTranslationMatrix(x: number, y: number, z: number): Matrix {
    return {
        x: { x: 1, y: 0, z: 0, t: x },
        y: { x: 0, y: 1, z: 0, t: y },
        z: { x: 0, y: 0, z: 1, t: z },
        t: { x: 0, y: 0, z: 0, t: 1 }
    };
}

export function parseTextSTLIntoData(data: string): string {
    let r: string = "";
    const lines = data
        .replace(/\r/g, "")
        .split("\n")
        .filter(x => x);

    const firstLine = lines.shift();
    if (!firstLine || !firstLine.startsWith("solid")) {
        throw new Error(`Wrong stl: ${firstLine}`);
    }

    while (true) {
        const [
            facet,
            outerLoop,
            vertex1,
            vertex2,
            vertex3,
            endloop,
            endFacet
        ] = [
            lines.shift(),
            lines.shift(),
            lines.shift(),
            lines.shift(),
            lines.shift(),
            lines.shift(),
            lines.shift()
        ];
        if (!facet) {
            break;
        }
        if (facet.indexOf("endsolid") > -1){
            break
        }
        if (!vertex1 || !vertex2 || !vertex3) {
            console.info( [
                facet,
                outerLoop,
                vertex1,
                vertex2,
                vertex3,
                endloop,
                endFacet
            ]);
            throw new Error(`Wrong stl vertex`);
        }
        const dot1 = vertex1
            .replace(/^\s*vertex\s+/, "")
            .replace(/\s*$/, "")
            .replace(/ /g, ",");
        const dot2 = vertex2
            .replace(/^\s*vertex\s+/, "")
            .replace(/\s*$/, "")
            .replace(/ /g, ",");
        const dot3 = vertex3
            .replace(/^\s*vertex\s+/, "")
            .replace(/\s*$/, "")
            .replace(/ /g, ",");
        r += `grey ${dot1} ${dot2} ${dot3} ${dot1}\n`;
    }
    return r;
}


export function isometricProjectionPoint(d: Dot) {
    return {
        x: d.x,
        y: -d.z
    };
}

export function perspectiveProjectionPoint(d: Dot, yDist = 2000, yCutOff = 100) {
    if (d.y < -yDist + yCutOff) {
        return undefined
    }
    const kX = d.x / (d.y + yDist);
    const bX = kX * yDist;
    const x = kX*d.x + bX;

    const kY = -d.z / (d.y + yDist);
    const bY = kY * yDist;
    const y = kY*(-d.z) + bY;

    return {
        x,
        y
    }
}
// import 'babel-polyfill';

import * as React from "react";
import * as ReactDOM from "react-dom";

import "bootstrap/dist/css/bootstrap.min.css";

// import './index.css';
import {
    parseRawDataIntoLines,
    PolyLine,
    isometricProjectionPoint,
    Matrix,
    getTranslationMatrix,
    vectorMatrixMul,
    getRotationMatrix,
    matrixMatrixMul,
    parseTextSTLIntoData,
    perspectiveProjectionPoint
} from "./engine";

const BASIS_RAW_DATA = `#ff0000 0,0,0 100,0,0
#00ff00 0,0,0 0,100,0
#0000ff 0,0,0 0,0,100
`;

const DEMO1_RAW_DATA = `
grey 0,0,0 200,0,0 200,200,0 0,200,0 0,0,0 0,0,200 200,0,200 200,200,200 0,200,200 0,0,200
grey 0,200,0 0,200,200
grey 200,200,0 200,200,200
grey 200,0,0 200,0,200
`;
const DEMO2_RAW_DATA = DEMO1_RAW_DATA + '\n' + 
    DEMO1_RAW_DATA.replace(/200/g,'150').replace(/ 0\,/g,' 100,')
    .replace(/grey/g,'green') + '\n' +
    DEMO1_RAW_DATA.replace(/200/g,'100').replace(/,0,/g,',200,')
    .replace(/grey/g,'blue');

type ProjectionType = "isometric" | "perspective";

interface RootState {
    tx: number;
    ty: number;
    tz: number;
    rx: number;
    ry: number;
    rz: number;
    scale: number;
    type: ProjectionType;
    rawData: string;
}

class Root extends React.Component<{}, RootState> {
    state: RootState = {
        tx: 0,
        ty: 0,
        tz: -200,
        rx: 30,
        ry: 0,
        rz: 20,
        scale: 1,
        type: "perspective",
        rawData: BASIS_RAW_DATA + "\n" + DEMO1_RAW_DATA
    };

    private loadStl = (name: string) => {
        fetch(name)
            .then(d => d.text())
            .then(stl => {
                const data1 = parseTextSTLIntoData(stl);
                this.setState({
                    rawData: BASIS_RAW_DATA + '\n' + data1
                });
            });
    };

    render() {
        const lines = parseRawDataIntoLines(this.state.rawData);

        return (
            <div className="m-3">
                <div className="row">
                    <div className="col-3">
                        <div className="form-group">
                            <label>RotateX = {this.state.rx}°</label>
                            <input
                                className="form-control"
                                type="range"
                                name="points"
                                id="points"
                                value={this.state.rx}
                                onChange={e =>
                                    this.setState({
                                        rx: parseInt(e.target.value)
                                    })
                                }
                                min="0"
                                max="360"
                            />
                        </div>

                        <div className="form-group">
                            <label>RotateY = {this.state.ry}°</label>
                            <input
                                className="form-control"
                                type="range"
                                name="points"
                                id="points"
                                value={this.state.ry}
                                onChange={e =>
                                    this.setState({
                                        ry: parseInt(e.target.value)
                                    })
                                }
                                min="0"
                                max="360"
                            />
                        </div>

                        <div className="form-group">
                            <label>RotateZ = {this.state.rz}°</label>
                            <input
                                className="form-control"
                                type="range"
                                name="points"
                                id="points"
                                value={this.state.rz}
                                onChange={e =>
                                    this.setState({
                                        rz: parseInt(e.target.value)
                                    })
                                }
                                min="0"
                                max="360"
                            />
                        </div>

                        <div className="form-group">
                            <label>Translate X = {this.state.tx}</label>
                            <input
                                className="form-control"
                                type="range"
                                name="points"
                                id="points"
                                value={this.state.tx}
                                onChange={e =>
                                    this.setState({
                                        tx: parseInt(e.target.value)
                                    })
                                }
                                min="0"
                                max="500"
                            />
                        </div>

                        <div className="form-group">
                            <label>Translate Y = {this.state.ty}</label>
                            <input
                                className="form-control"
                                type="range"
                                name="points"
                                id="points"
                                value={this.state.ty}
                                onChange={e =>
                                    this.setState({
                                        ty: parseInt(e.target.value)
                                    })
                                }
                                min="-1500"
                                max="1000"
                            />
                        </div>

                        <div className="form-group">
                            <label>Translate Z = {this.state.tz}</label>
                            <input
                                className="form-control"
                                type="range"
                                name="points"
                                id="points"
                                value={this.state.tz}
                                onChange={e =>
                                    this.setState({
                                        tz: parseInt(e.target.value)
                                    })
                                }
                                min="-500"
                                max="500"
                            />
                        </div>

                        <div className="form-group">
                            <label>Type</label>
                            <div className="radio ml-2">
                                <label>
                                    <input
                                        type="radio"
                                        value="option2"
                                        checked={
                                            this.state.type === "isometric"
                                        }
                                        onChange={e =>
                                            this.setState({ type: "isometric" })
                                        }
                                    />{" "}
                                    Isometric
                                </label>
                            </div>
                            <div className="radio ml-2">
                                <label>
                                    <input
                                        type="radio"
                                        value="option2"
                                        checked={
                                            this.state.type === "perspective"
                                        }
                                        onChange={e =>
                                            this.setState({
                                                type: "perspective"
                                            })
                                        }
                                    />{" "}
                                    Perspective
                                </label>
                            </div>
                        </div>

                        <div className="form-group">
                            <label className={!lines ? "text-warning" : ""}>
                                Input data
                            </label>
                            <textarea
                                className={"form-control"}
                                style={{
                                    height: "20em"
                                }}
                                value={this.state.rawData}
                                onChange={e =>
                                    this.setState({
                                        rawData: e.target.value
                                    })
                                }
                            />
                        </div>
                        <div>
                            <button
                                className="btn btn-light"
                                onClick={() =>
                                    this.setState({
                                        rawData:
                                            BASIS_RAW_DATA +
                                            "\n" +
                                            DEMO1_RAW_DATA
                                    })
                                }
                            >
                                Demo1
                            </button>
                            <button
                                className="btn btn-light"
                                onClick={() =>
                                    this.setState({
                                        rawData:
                                            BASIS_RAW_DATA +
                                            "\n" +
                                            DEMO2_RAW_DATA
                                    })
                                }
                            >
                                Demo2
                            </button>
                            <button
                                className="btn btn-light"
                                onClick={() =>
                                    this.loadStl("Fabulous_Albar.stl")
                                }
                            >
                                Demo3
                            </button>
                        </div>
                    </div>
                    <div className="col-9">
                        {lines ? (
                            <Projection
                                lines={lines}
                                type={this.state.type}
                                transformMatrix={matrixMatrixMul(
                                    getRotationMatrix(
                                        this.state.rx / 180 * Math.PI,
                                        this.state.ry / 180 * Math.PI,
                                        this.state.rz / 180 * Math.PI
                                    ),

                                    getTranslationMatrix(
                                        this.state.tx,
                                        this.state.ty,
                                        this.state.tz
                                    )
                                )}
                            />
                        ) : null}
                    </div>
                </div>
            </div>
        );
    }
}

class Projection extends React.Component<
    {
        lines: PolyLine[];
        type: ProjectionType;
        transformMatrix: Matrix;
    },
    {}
> {
    render() {
        return (
            <svg height="600" width="100%">
                {this.props.lines.map((line, index) => (
                    <polyline
                        key={index}
                        fill="none"
                        stroke={`${line.color}`}
                        points={line.dots
                            .map(d3d => {
                                const d3dTransformed = vectorMatrixMul(
                                    d3d,
                                    this.props.transformMatrix
                                );
                                const d2d =
                                    this.props.type === "isometric"
                                        ? isometricProjectionPoint(
                                              d3dTransformed
                                          )
                                        : perspectiveProjectionPoint(
                                              d3dTransformed
                                          );
                                return d2d
                                    ? `${d2d.x + 200},${d2d.y + 200}`
                                    : "";
                            })
                            .join(" ")}
                    />
                ))}
            </svg>
        );
    }
}

const root = document.getElementById("root");

ReactDOM.render(<Root />, root);
